using namespace std;

int search(string text, string pattern){
	bool patternFound = false;
	bool patternCorrect;
	unsigned int i = 0;
	unsigned int n;
	int indexLocation = -1;
	 
	while(((i + pattern.size()) < text.size())){
		if(patternFound == false){
			patternCorrect = true;
			n = 0;
			while(patternCorrect == true){
				if(n < (pattern.size()-1)){
					if(text[i+n] != pattern[n])
						patternCorrect = false;
					if(patternCorrect == true){
						if(n == (pattern.size() -1)){
							patternFound = true;
							indexLocation = i;
						}
					}
					n++;
				}
			}
			i++;
		}
	}
	return indexLocation;
}
